#include <QtGui>

#include "filetable.h"

FileTable::FileTable(QWidget *parent) : QTableWidget(parent)
{
    setDragDropMode(QAbstractItemView::DropOnly);
    setAlternatingRowColors(true);
    setAcceptDrops(true);
    setSelectionMode(QAbstractItemView::SingleSelection);
    setSelectionBehavior(QAbstractItemView::SelectRows);
}

void FileTable::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
    emit changed(event->mimeData());
}

void FileTable::dragMoveEvent(QDragMoveEvent *event)
{
    event->acceptProposedAction();
}

void FileTable::dropEvent(QDropEvent *event)
{
    event->acceptProposedAction();
    emit dropped(event->mimeData());
}

void FileTable::dragLeaveEvent(QDragLeaveEvent *event)
{
    event->accept();
}

void FileTable::clear()
{
    emit changed();
}

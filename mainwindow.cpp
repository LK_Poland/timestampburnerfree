#include "mainwindow.h"
#include "ui_mainwindow.h"

static const QString& AppVersion = "1.5";
static const QString& AppAuthor = "LK";
static const QString& AppWebSite = "http://my3soft.getenjoyment.net";

static const QString& TempFolder = "TempFolder";
static const QString& OutputFolder = "OutputFolder";
static const QString& ReadFrom = "ReadFrom";
static const QString& ReadFromFirstOnly = "ReadFromFirstOnly";
static const QString& MergeFiles = "MergeFiles";
static const QString& CustomText = "CustomText";
static const QString& CustomTextPos = "CustomTextPosition";
static const QString& CustomTextColor = "CustomTextColor";
static const QString& CustomTextScale = "CustomTextScale";
static const QString& CustomTextLineThickness = "CustomTextLineThickness";
static const QString& CustomTextMarginX = "CustomTextMarginX";
static const QString& CustomTextMarginY = "CustomTextMarginY";
static const QString& CustomTextFormat = "CustomTextFormat";
static const QString& CustomTextFormatDefault = "yyyy/MM/dd hh:mm:ss";
static const QString& Threads = "Threads";
static const QString& Preset = "Preset";
static const QString& QualityLevel = "QualityLevel";
static const QString& TextBurn = "Burn timestamp";
static const QString& TextCancel = "Cancel";

static const QStringList& ColumnLabels = {"Name", "Size", "Status"};
static const QStringList& ReadFromSources = {"metadata", "file name"};
static const QStringList& PresetNames = {"ultrafast", "superfast", "veryfast", "faster", "fast",
                                        "medium", "slow", "slower", "veryslow", "placebo"};

static const QString& Mp4Ext = "mp4";

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow),
      tbButton(this), settings("config.ini", QSettings::IniFormat)
{
    ui->setupUi(this);
    pshClear = new QPushButton("Clear list");
    pshMoveUp = new QPushButton("Move up");
    pshMoveUp->setMaximumWidth(100);
    pshMoveDown = new QPushButton("Move down");
    pshMoveDown->setMaximumWidth(100);
    tblFiles = new FileTable();
    pshBurn = new QPushButton(TextBurn);
    pshCheckForUpdates = new QPushButton("Check for updates...");
    pshCheckForUpdates->setMaximumWidth(200);
    tblFiles->setGeometry(10, 10, 400, 300);
    tblFiles->setColumnCount(3);
    tblFiles->setColumnWidth(0, 300);
    tblFiles->setColumnWidth(1, 190);
    tblFiles->setColumnWidth(2, 190);
    tblFiles->setHorizontalHeaderLabels(ColumnLabels);
    tblFiles->setEditTriggers(QAbstractItemView::NoEditTriggers);
    cmbReadFrom = new QComboBox();
    cmbReadFrom->addItems(ReadFromSources);
    cmbReadFrom->setCurrentText(settings.value(ReadFrom, ReadFromSources[0]).toString());
    chkReadFromFirstOnly = new QCheckBox();
    chkReadFromFirstOnly->setText("Read from first file only");
    chkReadFromFirstOnly->setChecked(settings.value(ReadFromFirstOnly, false).toBool());
    chkMerge = new QCheckBox();
    chkMerge->setText("Merge all files into single movie");
    chkMerge->setChecked(settings.value(MergeFiles, false).toBool());
    widCustomText = new QWidget();
    chkCustomText = new QCheckBox("Custom text");
    chkCustomText->setChecked(settings.value(CustomText, false).toBool());
    customTextChanged(chkCustomText->isChecked() ? Qt::Checked : Qt::Unchecked);
    cmbCustomTextPos = new QComboBox();
    cmbCustomTextPos->addItems(QStringList() << "bottom-left" << "top-left" << "top-right" << "bottom-right");
    cmbCustomTextPos->setCurrentIndex(settings.value(CustomTextPos, Burner::BottomLeft).toInt());
    pshCustomTextColor = new QPushButton("Color...");
    lblCustomTextColor = new QLabel();
    lblCustomTextColor->setFixedWidth(125);
    setCustomTextColor(settings.value(CustomTextColor, QColor(Qt::white).rgb()).toUInt());
    spnCustomTextScale = new QDoubleSpinBox();
    spnCustomTextScale->setRange(0, 100);
    spnCustomTextScale->setSingleStep(0.1);
    spnCustomTextScale->setValue(settings.value(CustomTextScale, 2).toDouble());
    spnCustomTextLineThickness = new QSpinBox();
    spnCustomTextLineThickness->setRange(0, 100);
    spnCustomTextLineThickness->setValue(settings.value(CustomTextLineThickness, 2).toInt());
    spnCustomTextMarginX = new QSpinBox();
    spnCustomTextMarginX->setRange(-200, 200);
    spnCustomTextMarginX->setValue(settings.value(CustomTextMarginX, 10).toInt());
    spnCustomTextMarginY = new QSpinBox();
    spnCustomTextMarginY->setRange(-200, 200);
    spnCustomTextMarginY->setValue(settings.value(CustomTextMarginY, 15).toInt());
    txtCustomTextFormat = new QLineEdit();
    txtCustomTextFormat->setText(settings.value(CustomTextFormat, CustomTextFormatDefault).toString());
    txtCustomTextFormat->setToolTip("Format -> Result\ndd.MM.yyyy -> 21.05.2001\nddd MMMM d yy -> Tue May 21 01\nhh:mm:ss.zzz -> 14:13:09.120\nhh:mm:ss.z -> 14:13:09.12\nh:m:s ap -> 2:13:9 pm");
    pshCustomTextFormatDefault = new QPushButton("Default");
    cmbThreads = new QComboBox();
    for (int i = 0 ; i <= QThread::idealThreadCount() ; ++i)
    {
        cmbThreads->addItem(QString::number(i));
    }
    cmbThreads->setCurrentText(settings.value(Threads, 0).toString());
    cmbPreset = new QComboBox();
    cmbPreset->addItems(PresetNames);
    cmbPreset->setCurrentText(settings.value(Preset, PresetNames[5]).toString());
    sldQuality = new QSlider(Qt::Horizontal);
    sldQuality->setRange(0, 51);
    sldQuality->setValue(settings.value(QualityLevel, 23).toInt());
    sldQuality->setToolTip(QString::number(sldQuality->value()));
    txtTemp = new QLineEdit();
    txtTemp->setEnabled(false);
    txtTemp->setText(settings.value(TempFolder, QDir::tempPath()).toString());
    txtOut = new QLineEdit();
    txtOut->setEnabled(false);
    txtOut->setText(settings.value(OutputFolder, QDir::homePath()).toString());
    pshSelectTemp = new QPushButton("...");
    pshSelectOut = new QPushButton("...");
    connect(pshClear, SIGNAL(clicked()), this, SLOT(clearClicked()));
    connect(pshMoveUp, SIGNAL(clicked()), this, SLOT(moveUpClicked()));
    connect(pshMoveDown, SIGNAL(clicked()), this, SLOT(moveDownClicked()));
    connect(cmbReadFrom, SIGNAL(currentTextChanged(const QString&)), this, SLOT(readFromChanged(const QString&)));
    connect(chkReadFromFirstOnly, SIGNAL(stateChanged(int)), this, SLOT(readFromFirstOnlyChanged(int)));
    connect(chkMerge, SIGNAL(stateChanged(int)), this, SLOT(mergeFilesChanged(int)));
    connect(chkCustomText, SIGNAL(stateChanged(int)), this, SLOT(customTextChanged(int)));
    connect(cmbCustomTextPos, SIGNAL(currentIndexChanged(int)), this, SLOT(customTextPosChanged(int)));
    connect(pshCustomTextColor, SIGNAL(clicked()), this, SLOT(customTextColorClicked()));
    connect(spnCustomTextScale, SIGNAL(valueChanged(double)), this, SLOT(customTextScaleChanged(double)));
    connect(spnCustomTextLineThickness, SIGNAL(valueChanged(int)), this, SLOT(customTextLineThicknessChanged(int)));
    connect(spnCustomTextMarginX, SIGNAL(valueChanged(int)), this, SLOT(customTextMarginXChanged(int)));
    connect(spnCustomTextMarginY, SIGNAL(valueChanged(int)), this, SLOT(customTextMarginYChanged(int)));
    connect(txtCustomTextFormat, SIGNAL(textChanged(const QString&)), this, SLOT(customTextFormatChanged(const QString&)));
    connect(pshCustomTextFormatDefault, SIGNAL(clicked()), this, SLOT(customTextFormatDefaultClicked()));
    connect(cmbThreads, SIGNAL(currentTextChanged(const QString&)), this, SLOT(threadsChanged(const QString&)));
    connect(cmbPreset, SIGNAL(currentTextChanged(const QString&)), this, SLOT(presetChanged(const QString&)));
    connect(sldQuality, SIGNAL(valueChanged(int)), this, SLOT(qualityChanged(int)));
    connect(&burner, SIGNAL(progress(int, int, qint64)), this, SLOT(burnProgress(int, int, qint64)));
    connect(&burner, SIGNAL(finished()), this, SLOT(burnFinished()));
    connect(tblFiles, SIGNAL(dropped(const QMimeData*)), this, SLOT(fileDropped(const QMimeData*)));
    connect(pshSelectTemp, SIGNAL(clicked()), this, SLOT(selectTempClicked()));
    connect(pshSelectOut, SIGNAL(clicked()), this, SLOT(selectOutClicked()));
    connect(pshBurn, SIGNAL(clicked()), this, SLOT(burnClicked()));
    connect(pshCheckForUpdates, SIGNAL(clicked()), this, SLOT(checkForUpdatesClicked()));
    QHBoxLayout* layUpper = new QHBoxLayout();
    layUpper->addWidget(pshClear);
    layUpper->addSpacerItem(new QSpacerItem(15, 5));
    layUpper->addWidget(pshMoveUp);
    layUpper->addWidget(pshMoveDown);
    QHBoxLayout* layTemp = new QHBoxLayout();
    layTemp->addWidget(new QLabel("Temp files location "));
    layTemp->addWidget(txtTemp);
    layTemp->addWidget(pshSelectTemp);
    QHBoxLayout* layOut = new QHBoxLayout();
    layOut->addWidget(new QLabel("Output files location"));
    layOut->addWidget(txtOut);
    layOut->addWidget(pshSelectOut);
    QHBoxLayout* layTimeStamp = new QHBoxLayout();
    layTimeStamp->setAlignment(Qt::AlignLeft);
    layTimeStamp->addWidget(new QLabel("Timestamp from"));
    layTimeStamp->addWidget(cmbReadFrom);
    layTimeStamp->addSpacerItem(new QSpacerItem(15, 5));
    layTimeStamp->addWidget(chkReadFromFirstOnly);
    layTimeStamp->addSpacerItem(new QSpacerItem(15, 5));
    layTimeStamp->addWidget(chkMerge);
    layTimeStamp->addSpacerItem(new QSpacerItem(15, 5));
    layTimeStamp->addWidget(chkCustomText);
    QHBoxLayout* layCustomTextHor1 = new QHBoxLayout();
    layCustomTextHor1->setAlignment(Qt::AlignLeft);
    layCustomTextHor1->addWidget(new QLabel("Position"));
    layCustomTextHor1->addWidget(cmbCustomTextPos);
    layCustomTextHor1->addSpacerItem(new QSpacerItem(15, 5));
    layCustomTextHor1->addWidget(pshCustomTextColor);
    layCustomTextHor1->addWidget(lblCustomTextColor);
    layCustomTextHor1->addSpacerItem(new QSpacerItem(15, 5));
    layCustomTextHor1->addWidget(new QLabel("Font scale"));
    layCustomTextHor1->addWidget(spnCustomTextScale);
    layCustomTextHor1->addSpacerItem(new QSpacerItem(15, 5));
    layCustomTextHor1->addWidget(new QLabel("Line thickness"));
    layCustomTextHor1->addWidget(spnCustomTextLineThickness);
    QHBoxLayout* layCustomTextHor2 = new QHBoxLayout();
    layCustomTextHor2->setAlignment(Qt::AlignLeft);
    layCustomTextHor2->addWidget(new QLabel("X margin"));
    layCustomTextHor2->addWidget(spnCustomTextMarginX);
    layCustomTextHor2->addSpacerItem(new QSpacerItem(15, 5));
    layCustomTextHor2->addWidget(new QLabel("Y margin"));
    layCustomTextHor2->addWidget(spnCustomTextMarginY);
    layCustomTextHor2->addSpacerItem(new QSpacerItem(15, 5));
    layCustomTextHor2->addWidget(new QLabel("Format"));
    layCustomTextHor2->addWidget(txtCustomTextFormat);
    layCustomTextHor2->addWidget(pshCustomTextFormatDefault);
    QVBoxLayout* layCustomText = new QVBoxLayout(widCustomText);
    layCustomText->setAlignment(Qt::AlignLeft);
    layCustomText->setContentsMargins(0, 0, 0, 0);
    layCustomText->addLayout(layCustomTextHor1);
    layCustomText->addLayout(layCustomTextHor2);
    QHBoxLayout* layQuality = new QHBoxLayout();
    layQuality->addWidget(new QLabel(Threads));
    layQuality->addWidget(cmbThreads);
    layQuality->addSpacerItem(new QSpacerItem(15, 5));
    layQuality->addWidget(new QLabel(Preset));
    layQuality->addWidget(cmbPreset);
    layQuality->addSpacerItem(new QSpacerItem(15, 5));
    layQuality->addWidget(new QLabel("Better quality"));
    layQuality->addWidget(sldQuality);
    layQuality->addWidget(new QLabel("Smaller size"));
    QHBoxLayout* layMainCustomText = new QHBoxLayout();
    layMainCustomText->setAlignment(Qt::AlignLeft);
    layMainCustomText->addWidget(widCustomText);
    QHBoxLayout* layBurn = new QHBoxLayout();
    layBurn->addWidget(pshBurn);
    layBurn->addSpacerItem(new QSpacerItem(15, 5));
    layBurn->addWidget(pshCheckForUpdates);
    QVBoxLayout* layMain = new QVBoxLayout();
    layMain->addLayout(layUpper);
    layMain->addWidget(tblFiles);
    layMain->addLayout(layTemp);
    layMain->addLayout(layOut);
    layMain->addLayout(layTimeStamp);
    layMain->addLayout(layMainCustomText);
    layMain->addLayout(layQuality);
    layMain->addLayout(layBurn);
    ui->widget->setLayout(layMain);
    statusBar()->showMessage(QString("ver. %1 (Qt %2) GPLv3, written by %3, site: %4").arg(AppVersion).arg(qVersion()).arg(AppAuthor).arg(AppWebSite));
}

MainWindow::~MainWindow()
{
    delete ui;
    settings.sync();
}

void MainWindow::showEvent(QShowEvent*)
{
    tbButton.setWindow(windowHandle());
    tbProgess = tbButton.progress();
    tbProgess->setRange(0, 100);
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    if (burner.isRunning())
    {
        QMessageBox::warning(this, "Warning", "Cannot exit while ongoing processing.",
                             QMessageBox::Ok);
        e->ignore();
    }
    else
    {
        e->accept();
    }
}

void MainWindow::clearList()
{
    while (tblFiles->rowCount())
    {
        tblFiles->removeRow(0);
    }
}

void MainWindow::clearClicked()
{
    clearList();
    files.clear();
}

void MainWindow::moveSelectedFile(const bool up)
{
    const int srcIdx = tblFiles->currentRow();
    if ((tblFiles->rowCount() < 2) ||
        (srcIdx < 0) ||
        (up && srcIdx == 0) ||
        (!up && srcIdx >= tblFiles->rowCount() - 1))
    {
        return;
    }
    const int dstIdx = up ? srcIdx - 1 : srcIdx + 1;
    files.move(srcIdx, dstIdx);
    clearList();
    Q_FOREACH(const QString& file, files)
    {
        addFile(file, true);
    }
    tblFiles->selectRow(dstIdx);
}

void MainWindow::moveUpClicked()
{
    moveSelectedFile(true);
}

void MainWindow::moveDownClicked()
{
    moveSelectedFile(false);
}

void MainWindow::readFromChanged(const QString &source)
{
    settings.setValue(ReadFrom, source);
    if (source == ReadFromSources[1])
    {
        QMessageBox::information(this, "Info",
                                 "File name must be in following format:\n\tyyyy-MM-ddThh;mm;ss");
    }
}

void MainWindow::readFromFirstOnlyChanged(int value)
{
    settings.setValue(ReadFromFirstOnly, value == Qt::Checked);
}

void MainWindow::mergeFilesChanged(int value)
{
    settings.setValue(MergeFiles, value == Qt::Checked);
}

void MainWindow::customTextChanged(int value)
{
    widCustomText->setEnabled(value == Qt::Checked);
    settings.setValue(CustomText, chkCustomText->isChecked());
}

void MainWindow::customTextPosChanged(int index)
{
    settings.setValue(CustomTextPos, index);
}

void MainWindow::setCustomTextColor(const QRgb color)
{
    customTextColor = QColor(color);
    const QVariant variant = customTextColor;
    lblCustomTextColor->setStyleSheet("QLabel { background-color:" + variant.toString() + "; }");
    settings.setValue(CustomTextColor, color);
}

void MainWindow::customTextColorClicked()
{
    QColorDialog cd(this);
    cd.setCurrentColor(customTextColor);
    if (QDialog::Rejected == cd.exec())
    {
        return;
    }
    setCustomTextColor(cd.selectedColor().rgb());
}

void MainWindow::customTextScaleChanged(double value)
{
    settings.setValue(CustomTextScale, QString::number(value, 'f', 2));
}

void MainWindow::customTextLineThicknessChanged(int value)
{
    settings.setValue(CustomTextLineThickness, value);
}

void MainWindow::customTextMarginXChanged(int value)
{
    settings.setValue(CustomTextMarginX, value);
}

void MainWindow::customTextMarginYChanged(int value)
{
    settings.setValue(CustomTextMarginY, value);
}

void MainWindow::customTextFormatChanged(const QString &text)
{
    settings.setValue(CustomTextFormat, text);
}

void MainWindow::customTextFormatDefaultClicked()
{
    txtCustomTextFormat->setText(CustomTextFormatDefault);
}

void MainWindow::threadsChanged(const QString &threads)
{
    settings.setValue(Threads, threads);
}

void MainWindow::presetChanged(const QString &preset)
{
    settings.setValue(Preset, preset);
}

void MainWindow::qualityChanged(int value)
{
    statusBar()->showMessage(QString("Current quality level is %1").arg(value));
    sldQuality->setToolTip(QString::number(value));
    settings.setValue(QualityLevel, value);
}

void MainWindow::burnClicked()
{
    if (pshBurn->text() == TextBurn)
    {
        QDir().mkdir(txtTemp->text());
        QDir().mkdir(txtOut->text());
        if (files.empty())
        {
            return;
        }
        if (chkMerge->isChecked())
        {
            showProgess("Merging", 0, files.count() - 1);
        }
        pshBurn->setText(TextCancel);
        changeUiState(false);
        burner.setTempFolder(txtTemp->text());
        burner.setOutFolder(txtOut->text());
        burner.setReadFromMeta(cmbReadFrom->currentIndex() == 0);
        burner.setReadFromFirstOnly(chkReadFromFirstOnly->isChecked());
        burner.setMerge(chkMerge->isChecked());
        burner.setPreset(cmbPreset->currentText());
        burner.setThreads(cmbThreads->currentText().toInt());
        burner.setQuality(sldQuality->value());
        burner.setFiles(files);
        burner.setTextFormat(chkCustomText->isChecked() && !txtCustomTextFormat->text().isEmpty() ?
                                 txtCustomTextFormat->text() : CustomTextFormatDefault);
        burner.setTextPos(chkCustomText->isChecked() ?
                              (Burner::TextPos)cmbCustomTextPos->currentIndex() : Burner::BottomLeft);
        burner.setTextScale(chkCustomText->isChecked() ? spnCustomTextScale->value() : -1);
        burner.setTextThickness(chkCustomText->isChecked() ? spnCustomTextLineThickness->value() : -1);
        burner.setTextColor(chkCustomText->isChecked() ? customTextColor : Qt::white);
        burner.setTextMargins(chkCustomText->isChecked() ? spnCustomTextMarginX->value() : 10,
                              chkCustomText->isChecked() ? spnCustomTextMarginY->value() : 15);
        burner.start();
    }
    else
    {
        if (QMessageBox::Yes == QMessageBox::question(this, "Confirm", "Abort burning process?",
                                                      QMessageBox::Yes | QMessageBox::No, QMessageBox::No))
        {
            pshBurn->setEnabled(false);
            burner.cancel();
        }
    }
}

void MainWindow::checkForUpdatesClicked()
{
    QDesktopServices::openUrl(AppWebSite + "/viewforum.php?f=4");
}

void MainWindow::burnFinished()
{
    pshBurn->setText(TextBurn);
    changeUiState(true);
}

void MainWindow::changeUiState(bool enabled)
{
    tbProgess->setVisible(!enabled);
    pshClear->setEnabled(enabled);
    pshMoveUp->setEnabled(enabled);
    pshMoveDown->setEnabled(enabled);
    tblFiles->setEnabled(enabled);
    pshSelectTemp->setEnabled(enabled);
    pshSelectOut->setEnabled(enabled);
    cmbReadFrom->setEnabled(enabled);
    chkReadFromFirstOnly->setEnabled(enabled);
    chkMerge->setEnabled(enabled);
    widCustomText->setEnabled(enabled && chkCustomText->isChecked());
    chkCustomText->setEnabled(enabled);
    cmbThreads->setEnabled(enabled);
    cmbPreset->setEnabled(enabled);
    sldQuality->setEnabled(enabled);
    pshCheckForUpdates->setEnabled(enabled);
    pshBurn->setEnabled(true);
}

void MainWindow::selectTempClicked()
{
    const QString& path = QFileDialog::getExistingDirectory(this, "Select temp folder", txtTemp->text());
    if (!path.isEmpty())
    {
        txtTemp->setText(path);
        settings.setValue(TempFolder, path);
    }
}

void MainWindow::selectOutClicked()
{
    const QString& path = QFileDialog::getExistingDirectory(this, "Select out folder", txtOut->text());
    if (!path.isEmpty())
    {
        txtOut->setText(path);
        settings.setValue(OutputFolder, path);
    }
}

void MainWindow::showProgess(const QString& text, const int first, const int last)
{
    for (int i = first ; i <= last ; ++i)
    {
        tblFiles->item(i, 2)->setText(text);
    }
}

void MainWindow::burnProgress(int index, int percent, qint64 eta)
{
    const qint64 eta_h = eta / 3600;
    const qint64 eta_m = (eta % 3600) / 60;
    const qint64 eta_s = eta % 60;
    QString text;
    if (percent != 100)
    {
        if (percent < 0)
        {
            text = "Canceled";
        }
        else
        {
            text = QString("%1% ETA: ").arg(percent);
            if (eta_h)
            {
                text += QString::number(eta_h) + "h ";
            }
            if (eta_m)
            {
                text += QString::number(eta_m) + "m ";
            }
            text += QString::number(eta_s) + "s";
        }
    }
    else
    {
        text = "Done";
    }
    if (chkMerge->isChecked())
    {
        showProgess(text, 0, files.count() - 1);
    }
    else
    {
        showProgess(text, index, index);
    }
    tbProgess->setValue(percent);
}

void MainWindow::fileDropped(const QMimeData *mime)
{
    if (!mime->hasUrls())
    {
        return;
    }
    for (const auto& url : mime->urls())
    {
        if (!url.isLocalFile())
        {
            continue;
        }
        const QString& path = url.toLocalFile();
        const QFileInfo fi(path);
        if (fi.isFile())
        {
            addFile(path, false);
        }
        else if (fi.isDir())
        {
            addDir(path);
        }
    }
}

void MainWindow::addFile(const QString &path, const bool tableOnly)
{
    QFileInfo fileInfo(path);
    if (fileInfo.suffix().toLower() != Mp4Ext)
    {
        return;
    }
    const int row = tblFiles->rowCount();
    tblFiles->insertRow(row);
    QTableWidgetItem* twi = new QTableWidgetItem();
    twi->setText(fileInfo.fileName());
    tblFiles->setItem(row, 0, twi);
    twi = new QTableWidgetItem();
    twi->setText(QString("%1 MiB").arg(fileInfo.size() / 1024 / 1024));
    tblFiles->setItem(row, 1, twi);
    twi = new QTableWidgetItem();
    twi->setText("Waiting");
    tblFiles->setItem(row, 2, twi);
    if (!tableOnly)
    {
        files << path;
    }
}

void MainWindow::addDir(const QString &path)
{
    const QDir dir(path);
    const QStringList& files = dir.entryList(QDir::Files, QDir::Name);
    for (const QString& file : files)
    {
        addFile(dir.filePath(file), false);
    }
}

#include "burner.h"

#include <QDebug>
#include <QDataStream>
#include <QElapsedTimer>
#include <QFileInfo>

#include "x264enc.h"

Burner::Burner()
{

}

void Burner::setTempFolder(const QString &path)
{
    tempFolder = path;
}

void Burner::setOutFolder(const QString &path)
{
    outFolder = path;
}

void Burner::setFiles(const QStringList &files)
{
    this->files = files;
}

void Burner::setPreset(const QString &preset)
{
    this->preset = preset;
}

void Burner::setThreads(const int threads)
{
    this->threads = threads;
}

void Burner::setQuality(const int quality)
{
    this->quality = quality;
}

void Burner::setTextFormat(const QString& format)
{
    this->textFormat = format;
}

void Burner::setTextPos(const TextPos pos)
{
    this->textPos = pos;
}

void Burner::setTextScale(const double scale)
{
    this->textScale = scale;
}

void Burner::setTextThickness(const int thickness)
{
    this->textThickness = thickness;
}

void Burner::setTextColor(const QColor& color)
{
    this->textColor = color;
}

void Burner::setTextMargins(const int x, const int y)
{
    this->textMarginX = x;
    this->textMarginY = y;
}

void Burner::setReadFromMeta(const bool value)
{
    this->readFromMeta = value;
}

void Burner::setReadFromFirstOnly(const bool value)
{
    this->readFromFirstOnly = value;
}

void Burner::setMerge(const bool value)
{
    this->merge = value;
}

void Burner::cancel()
{
    canceled = true;
}

static const QString& FFMPEG = "ffmpeg/ffmpeg.exe";
static const QString& FFPROBE = "ffmpeg/ffprobe.exe";

void Burner::logProcessOutput(QProcess& proc, const QString& file)
{
    QFile log(file);
    log.open(QFile::WriteOnly);
    QTextStream ts(&log);
    ts << "STDOUT:\n" << proc.readAllStandardOutput();
    ts << "\nSTDERR:\n" << proc.readAllStandardError();
    log.close();
}

QString Burner::readTimeStamp(const QString& file)
{
    if (readFromMeta)
    {
        QStringList ffprobeArgs;
        ffprobeArgs << "-v" << "quiet" << "-show_entries" << "stream_tags=creation_time" << file;
        QProcess ffprobe;
        ffprobe.start(FFPROBE, ffprobeArgs);
        ffprobe.waitForFinished();
        const QString& ffprobeOut = ffprobe.readAll();
        return ffprobeOut.mid(ffprobeOut.indexOf("creation_time") + 14, 19);
    }
    else
    {
        return QFileInfo(file).baseName().replace(';', ':');
    }
}

void Burner::run()
{
    canceled = false;
    const QString& tempVideo = tempFolder + "\\temp.h264";
    const QString& tempAudio = tempFolder + "\\temp.aac";
    const QString& tempList  = tempFolder + "\\vlist.txt";
    QString tempMerge = tempFolder;
    if (merge && files.count())
    {
        QFile list(tempList);
        list.open(QFile::WriteOnly);
        QTextStream lst(&list);
        for (int j = 0 ; j != files.size(); ++j)
        {
            lst << "file '" << files[j] << "'\n";
        }
        list.close();
        const QDateTime& timeStamp = QDateTime::fromString(readTimeStamp(files[0]), Qt::ISODate);
        tempMerge += QString("\\%1.mp4").arg(QString(timeStamp.toString(Qt::ISODate).replace(":", ";")));
        if (QFile(tempMerge).exists())
        {
            emit progress(0, -1, 0);
            return;
        }
        QStringList ffmpegMergeArgs;
        ffmpegMergeArgs << "-f" << "concat" << "-safe" << "0" << "-i" << tempList <<
                           "-metadata" << QString("creation_time=%1").arg(timeStamp.addSecs(timeStamp.offsetFromUtc()).toString(Qt::ISODate)) <<
                           "-c" << "copy" << tempMerge;
        QProcess ffmpegMerge;
        ffmpegMerge.start(FFMPEG, ffmpegMergeArgs);
        ffmpegMerge.waitForFinished(99999999);
        files.clear();
        files.append(tempMerge);
    }
    QElapsedTimer timer;
    QDateTime dateTime;
    QString creationDateTime;
    for (int j = 0 ; j != files.size(); ++j)
    {
        const QString& file = files[j];
        const QFileInfo info(file);
        const QString& outVideo =
                QString("%1\\%2-with-timestamp.%3").arg(outFolder).arg(info.baseName()).arg(info.suffix());
        if (QFile(outVideo).exists())
        {
            emit progress(j, -1, 0);
            continue;
        }
        if (!readFromFirstOnly || !j)
        {
            creationDateTime = readTimeStamp(file);
            dateTime = QDateTime::fromString(creationDateTime, Qt::ISODate);
        }
        cv::VideoCapture input(file.toStdString());
        if (!input.isOpened())
        {
            return;
        }
        const double fpsDouble = input.get(cv::CAP_PROP_FPS);
        const int fpsInt = fpsDouble + 0.5;
        const int width = input.get(cv::CAP_PROP_FRAME_WIDTH);
        const int height = input.get(cv::CAP_PROP_FRAME_HEIGHT);
        const int totalFrames = input.get(cv::CAP_PROP_FRAME_COUNT);
        QFile outFile(tempVideo);
        outFile.open(QFile::WriteOnly);
        QDataStream output(&outFile);
        X264Enc x264(width, height, threads, fpsDouble, quality, preset, &output);
        uint32_t frames = 0;
        cv::Mat frame;
        qInfo() << "Timestamp:" << dateTime.toString(Qt::ISODate).toStdString().c_str();
        timer.restart();
        for (int i = 0 ; i != totalFrames ; ++i)
        {
            input.read(frame);
            if (frame.empty())
            {
                qDebug() << "Empty frame!";
                continue;
            }
            const std::string& text = dateTime.toString(textFormat).toStdString();
            if (textScale < 0 || textThickness < 0)
            {
                textScale = height > 1080 ? 2 : 1;
                textThickness = textScale;
                textColor = Qt::white;
            }
            int baseLine = 0;
            const cv::Size& textSize = cv::getTextSize(text, cv::FONT_HERSHEY_DUPLEX,
                                                       textScale, textThickness, &baseLine);
            cv::Point origin;
            switch (textPos)
            {
                case BottomLeft:
                    origin = cv::Point(textMarginX, height - textSize.height + textMarginY);
                break;
                case TopLeft:
                    origin = cv::Point(textMarginX, textSize.height + textMarginY);
                break;
                case TopRight:
                    origin = cv::Point(width - textSize.width - textMarginX, textSize.height + textMarginY);
                break;
                case BottomRight:
                    origin = cv::Point(width - textSize.width - textMarginX,
                                       height - textSize.height + textMarginY);
                break;
            }
            cv::putText(frame, text, origin, cv::FONT_HERSHEY_DUPLEX,
                        textScale, cv::Scalar(textColor.blue(), textColor.green(), textColor.red()),
                        textThickness, cv::LINE_AA, false);
            x264.Encode(frame);
            if (i % fpsInt == 0)
            {
                if (canceled)
                {
                    emit progress(j, -1, 0);
                    break;
                }
                dateTime = dateTime.addSecs(1);
                const qint64 elapsed = timer.elapsed() / 1000;
                const double percent = (i + 1.0) * 100.0 / totalFrames;
                const double speed = (i + 1.0) / (elapsed + 1.0);
                const double eta = (totalFrames - i - 1.0) / speed;
                emit progress(j, percent, eta);
            }
            ++frames;
        }
        x264.Flush();
        input.release();
        outFile.close();
        QProcess ffmpegAudio;
        QStringList ffmpegAudioArgs;
        ffmpegAudioArgs << "-i" << file << "-vn" << "-acodec" << "copy" << tempAudio;
        ffmpegAudio.start(FFMPEG, ffmpegAudioArgs);
        ffmpegAudio.waitForFinished(99999999);
        QProcess ffmpegMerge;
        QStringList ffmpegMergeArgs;
        ffmpegMergeArgs << "-i" << tempVideo << "-i" << tempAudio << "-map" << "0:v" << "-map" << "1:a"
                        << "-metadata" << QString("creation_time=%1").arg(QDateTime::fromString(creationDateTime, Qt::ISODate).addSecs(dateTime.offsetFromUtc()).toString(Qt::ISODate))
                        << "-c" << "copy" << "-shortest" << outVideo;
        ffmpegMerge.start(FFMPEG, ffmpegMergeArgs);
        ffmpegMerge.waitForFinished(99999999);
        if (file == tempMerge)
        {
            QFile(tempMerge).remove();
            QFile(tempList).remove();
        }
        QFile(tempAudio).remove();
        QFile(tempVideo).remove();
        if (!canceled)
        {
            emit progress(j, 100, 0);
        }
    }
}

QT       += core gui winextras

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

INCLUDEPATH += D:/opencv-4.5.3/build/install/include D:/x264-master
LIBS += -L"D:/opencv-4.5.3/build/install/x64/mingw/lib" -lopencv_core453 -lopencv_highgui453 -lopencv_imgcodecs453 -lopencv_imgproc453 -lopencv_video453 -lopencv_videoio453 -L"D:/x264-master" -lx264-164

RC_ICONS = timestamp.ico

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    burner.cpp \
    filetable.cpp \
    main.cpp \
    mainwindow.cpp \
    x264enc.cpp

HEADERS += \
    burner.h \
    filetable.h \
    mainwindow.h \
    x264enc.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#ifndef FILETABLE_H
#define FILETABLE_H

#include <QPushButton>
#include <QTableWidget>

class QMimeData;

class FileTable : public QTableWidget
{
    Q_OBJECT

    public:
        FileTable(QWidget *parent = 0);

    public slots:
        void clear();

    signals:
        void changed(const QMimeData *mimeData = 0);
        void dropped(const QMimeData *mimeData = 0);

    protected:
        void dragEnterEvent(QDragEnterEvent *event) override;
        void dragMoveEvent(QDragMoveEvent *event) override;
        void dragLeaveEvent(QDragLeaveEvent *event) override;
        void dropEvent(QDropEvent *event) override;
};
#endif // FILETABLE_H

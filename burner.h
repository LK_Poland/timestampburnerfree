#ifndef BURNER_H
#define BURNER_H

#include <QDateTime>
#include <QMessageBox>
#include <QProcess>
#include <QThread>
#include <QUrl>

#include <opencv2/opencv.hpp>

class Burner : public QThread
{
    Q_OBJECT

    public:
        enum TextPos
        {
            BottomLeft,
            TopLeft,
            TopRight,
            BottomRight
        };

        Burner();

        void setTempFolder(const QString& path);
        void setOutFolder(const QString& path);
        void setFiles(const QStringList& files);
        void setPreset(const QString& preset);
        void setThreads(const int threads);
        void setQuality(const int quality);
        void setTextFormat(const QString& format);
        void setTextPos(const TextPos pos);
        void setTextScale(const double scale);
        void setTextThickness(const int thickness);
        void setTextColor(const QColor& color);
        void setTextMargins(const int x, const int y);
        void setReadFromMeta(const bool value);
        void setReadFromFirstOnly(const bool value);
        void setMerge(const bool value);

        void cancel();

    signals:
        void progress(int index, int percent, qint64 eta);

    protected:
        void run() override;
        QString readTimeStamp(const QString &file);
        void logProcessOutput(QProcess &proc, const QString &file);

    private:
        QString tempFolder;
        QString outFolder;
        QStringList files;
        QString preset;
        int threads;
        int quality;
        QString textFormat;
        TextPos textPos;
        double textScale;
        int textThickness;
        QColor textColor;
        int textMarginX;
        int textMarginY;
        bool readFromMeta;
        bool readFromFirstOnly;
        bool merge;
        bool canceled;
};

#endif // BURNER_H

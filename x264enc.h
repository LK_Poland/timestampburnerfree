#ifndef X264_H
#define X264_H

#include <QDataStream>

#include <opencv2/opencv.hpp>
#include <x264.h>

class X264Enc
{
    public:
        X264Enc(const int width, const int height, const int threads, const double fps, const int quality,
                const QString& preset, QDataStream *stream);
        ~X264Enc();

        void Encode(const cv::Mat& frame);
        void Flush();

    private:
        void Write();

    private:
        QDataStream* stream;
        x264_t *enc;
        x264_nal_t *nal;
        x264_param_t param;
        x264_picture_t pic;
        x264_picture_t pic_out;
        int i_frame;
        int i_frame_size;
        int i_nal;
        int luma_size;
        int chroma_size;
};

#endif // X264_H

#include "x264enc.h"

#include <QDebug>

X264Enc::X264Enc(const int width, const int height, const int threads, const double fps, const int quality,
                 const QString& preset, QDataStream* output)
    : stream(output)
{
    qInfo(qPrintable(QString("H.264: %1x%2 %3fps rfc=%4 preset=%5").arg(width).arg(height).arg(fps).arg(quality).arg(preset)));

    luma_size = width * height;
    chroma_size = luma_size / 4;
    i_frame = i_frame_size = 0;

    /* Get default params for preset/tuning */
    x264_param_default_preset(&param, preset.toStdString().c_str(), nullptr);

    param.i_threads = threads;
    param.i_lookahead_threads = threads;
    param.b_deterministic = 1;
    param.i_sync_lookahead = X264_SYNC_LOOKAHEAD_AUTO;
    param.i_bitdepth = 8;
    //param.i_level_idc = 51;
    param.i_csp = X264_CSP_I420;
    param.i_width  = width;
    param.i_height = height;
    param.i_frame_total = 0;
    //param.i_keyint_max = fps;
    //param.i_bframe = 5;
    //param.b_open_gop = 0;
    //param.i_bframe_pyramid = 0;
    //param.i_bframe_adaptive = X264_B_ADAPT_TRELLIS;
    param.b_vfr_input = 0;
    param.b_repeat_headers = 1;
    param.b_annexb = 1;
    param.i_fps_num = fps * 10000.0;
    param.i_fps_den = 10000;
    param.rc.f_rf_constant = quality;
    param.rc.i_rc_method = X264_RC_CRF;

    /* Apply profile restrictions. */
    x264_param_apply_profile(&param, "high444");

    x264_picture_alloc(&pic, param.i_csp, param.i_width, param.i_height);
    pic.img.i_plane = 3;
    pic.i_type = X264_TYPE_AUTO;

    enc = x264_encoder_open(&param);
}

X264Enc::~X264Enc()
{
    x264_encoder_close(enc);
    x264_picture_clean(&pic);
}

void X264Enc::Encode(const cv::Mat& frame)
{
    cv::Mat bgr(frame), yuv;

    if (1 == frame.channels())
    {
        cv::cvtColor(frame, bgr, cv::COLOR_GRAY2BGR);
    }

    cv::cvtColor(bgr, yuv, cv::COLOR_BGR2YUV_I420);

    memcpy(pic.img.plane[0], yuv.data, luma_size);
    memcpy(pic.img.plane[1], yuv.data + luma_size, chroma_size);
    memcpy(pic.img.plane[2], yuv.data + luma_size + chroma_size, chroma_size);

    pic.i_pts = i_frame++;

    i_frame_size = x264_encoder_encode(enc, &nal, &i_nal, &pic, &pic_out);
    Write();
}

void X264Enc::Flush()
{
    while (x264_encoder_delayed_frames(enc))
    {
        i_frame_size = x264_encoder_encode(enc, &nal, &i_nal, nullptr, &pic_out);
        Write();
    }
}

void X264Enc::Write()
{
    if (i_frame_size > 0)
    {
        stream->writeRawData((const char*)nal->p_payload, i_frame_size);
    }
    else if (i_frame_size < 0)
    {
        qDebug("Encoder failed!");
    }
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QCheckBox>
#include <QCloseEvent>
#include <QColorDialog>
#include <QComboBox>
#include <QDesktopServices>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QMimeData>
#include <QSettings>
#include <QSpinBox>
#include <QSlider>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QHBoxLayout>
#include <QStatusBar>
#include <QWinTaskbarButton>
#include <QWinTaskbarProgress>

#include "burner.h"
#include "filetable.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    protected:
        void showEvent(QShowEvent* e) override;
        void closeEvent(QCloseEvent *e) override;

    private slots:
        void clearList();
        void clearClicked();
        void moveSelectedFile(const bool up);
        void moveUpClicked();
        void moveDownClicked();
        void readFromChanged(const QString& source);
        void readFromFirstOnlyChanged(int value);
        void mergeFilesChanged(int value);
        void customTextChanged(int value);
        void customTextPosChanged(int index);
        void setCustomTextColor(const QRgb color);
        void customTextColorClicked();
        void customTextScaleChanged(double value);
        void customTextLineThicknessChanged(int value);
        void customTextMarginXChanged(int value);
        void customTextMarginYChanged(int value);
        void customTextFormatChanged(const QString& text);
        void customTextFormatDefaultClicked();
        void threadsChanged(const QString& threads);
        void presetChanged(const QString& preset);
        void qualityChanged(int value);
        void burnClicked();
        void checkForUpdatesClicked();
        void selectTempClicked();
        void selectOutClicked();
        void burnProgress(int index, int percent, qint64 eta);
        void fileDropped(const QMimeData *mime);
        void addFile(const QString &path, const bool tableOnly);
        void addDir(const QString &path);
        void burnFinished();
        void changeUiState(bool enabled);
        void showProgess(const QString &text, const int first, const int last);

    private:
        Ui::MainWindow *ui;
        QWinTaskbarButton tbButton;
        QWinTaskbarProgress* tbProgess;
        QSettings settings;
        QComboBox* cmbReadFrom;
        QCheckBox* chkReadFromFirstOnly;
        QCheckBox* chkMerge;
        QCheckBox* chkCustomText;
        QWidget* widCustomText;
        QComboBox* cmbCustomTextPos;
        QPushButton* pshCustomTextColor;
        QLabel* lblCustomTextColor;
        QDoubleSpinBox* spnCustomTextScale;
        QSpinBox* spnCustomTextLineThickness;
        QSpinBox* spnCustomTextMarginX;
        QSpinBox* spnCustomTextMarginY;
        QLineEdit* txtCustomTextFormat;
        QPushButton* pshCustomTextFormatDefault;
        QComboBox* cmbThreads;
        QComboBox* cmbPreset;
        QSlider* sldQuality;
        QPushButton* pshClear;
        QPushButton* pshMoveUp;
        QPushButton* pshMoveDown;
        QPushButton* pshBurn;
        QPushButton* pshCheckForUpdates;
        QPushButton* pshSelectTemp;
        QPushButton* pshSelectOut;
        QLineEdit* txtTemp;
        QLineEdit* txtOut;
        FileTable* tblFiles;
        QColor customTextColor;
        QStringList files;
        Burner burner;
};
#endif // MAINWINDOW_H
